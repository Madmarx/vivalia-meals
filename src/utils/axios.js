import Axios from 'axios'

const axios = Axios.create({
  baseURL: 'https://' + window.location.hostname + ':1338/api/',
  timeout: 10000,
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': '*',
    'Access-Control-Allow-Headers': '*',
    'x-access-token': localStorage.getItem('jwt')
  }
})

export default axios
