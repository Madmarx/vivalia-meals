import MainStore from '@/store/Store'
import axios from '@/utils/axios'

export default {
  namespaced: true,
  state: {
    menus: []
  },
  mutations: {
    LOAD_MENUS (state, menus) {
      state.menus = menus
    },
    ADD_MENU (state, menu) {
      state.menus.push(menu)
    },
    REMOVE_MENU (state, id) {
      state.menus = state.menus.filter(m => m._id !== id)
    },
    UPDATE_MENU: (state, menu) => {
      state.menus.forEach(m => {
        if (m._id === menu._id) {
          m = menu
          return null
        }
      })
    }
  },
  getters: {
    menus: state => state.menus,
    count: state => state.menus.length,
    menuById: (state) => (id) => {
      return state.menus.find(u => u._id === id)
    }
  },
  actions: {
    loadMenus: (store) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.get('/menus')
          .then(res => {
            store.commit('LOAD_MENUS', res.data)
            resolve(res.data)
          })
          .catch(err => {
            console.error(err)
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    addMenu: (store, menu) => {
      return new Promise((resolve, reject) => {
        axios.put('/menus', menu)
          .then(res => {
            store.commit('ADD_MENU', res.data)
            resolve(res.data)
          })
          .catch(err => {
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    removeMenu: (store, id) => {
      return new Promise((resolve, reject) => {
        axios.delete('/menus/' + id)
          .then(res => {
            store.commit('REMOVE_MENU', id)
            resolve(res.data)
          })
          .catch(err => {
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    updateMenu: (store, menu) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.put('/menus/' + menu._id, menu)
          .then(res => {
            store.commit('UPDATE_MENU', menu)
            resolve(res.data)
          })
          .catch(err => {
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    }
  }
}
