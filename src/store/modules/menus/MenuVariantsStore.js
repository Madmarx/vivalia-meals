import MainStore from '@/store/Store'
import axios from '@/utils/axios'

export default {
  namespaced: true,
  state: {
    menuVariants: []
  },
  mutations: {
    LOAD_MENU_VARIANTS (state, menuVariants) {
      state.menuVariants = menuVariants
    },
    ADD_MENU_VARIANT (state, menuVariant) {
      state.menuVariants.push(menuVariant)
    },
    REMOVE_MENU_VARIANT (state, id) {
      state.menuVariants = state.menuVariants.filter(m => m._id !== id)
    },
    UPDATE_MENU_VARIANT: (state, menuVariant) => {
      state.menuVariants.forEach(m => {
        if (m._id === menuVariant._id) {
          m = menuVariant
          return null
        }
      })
    }
  },
  getters: {
    menuVariants: state => state.menuVariants,
    count: state => state.menuVariants.length
  },
  actions: {
    loadMenuVariants: (store, idMenuBase) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.get('/menu-variants' + idMenuBase)
          .then(res => {
            store.commit('LOAD_MENU_VARIANTS', res.data)
            resolve(res.data)
          })
          .catch(err => {
            console.error(err)
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    loadMenuVariantsOfMenu: (store, idMenuBase) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.get('/menu-variants/menu/' + idMenuBase)
          .then(res => {
            // res.data.forEach(m => {
            //   store.commit('ADD_MENU_VARIANT', m)
            // })
            store.commit('LOAD_MENU_VARIANTS', res.data)
            resolve(res.data)
          })
          .catch(err => {
            console.error(err)
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    addMenuVariant: (store, menuVariant) => {
      return new Promise((resolve, reject) => {
        axios.put('/menu-variants', menuVariant)
          .then(res => {
            store.commit('ADD_MENU_VARIANT', res.data)
            resolve(res.data)
          })
          .catch(err => {
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    removeMenuVariant: (store, id) => {
      return new Promise((resolve, reject) => {
        axios.delete('/menu-variants/' + id)
          .then(res => {
            store.commit('REMOVE_MENU_VARIANT', id)
            resolve(res.data)
          })
          .catch(err => {
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    updateMenuVariant: (store, menuVariant) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.put('/menu-variants/' + menuVariant._id, menuVariant)
          .then(res => {
            store.commit('UPDATE_MENU_VARIANT', menuVariant)
            resolve(res.data)
          })
          .catch(err => {
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    }
  }
}
