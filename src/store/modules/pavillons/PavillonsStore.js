import MainStore from '@/store/Store'
import axios from '@/utils/axios'

export default {
  namespaced: true,
  state: {
    pavillons: []
  },
  mutations: {
    LOAD_PAVILLONS (state, pavillons) {
      state.pavillons = pavillons
    },
    LOAD_PAVILLON (state, pavillon) {
      state.pavillons = [pavillon]
    },
    ADD_PAVILLON (state, pavillon) {
      state.pavillons.push(pavillon)
    },
    REMOVE_PAVILLON (state, id) {
      state.pavillons = state.pavillons.filter(p => p._id !== id)
    },
    UPDATE_PAVILLON: (state, pavillon) => {
      state.pavillons.forEach(p => {
        if (p._id === pavillon._id) {
          p = pavillon
          return null
        }
      })
    },
    SET_VALIDATED_PAVILLON: (state, pavillon) => {
      state.pavillons.find(p => p._id === pavillon._id).validated = true
    }
  },
  getters: {
    pavillons: state => state.pavillons,
    count: state => state.pavillons.length
  },
  actions: {
    loadPavillons: (store) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.get('/pavillons')
          .then(res => {
            store.commit('LOAD_PAVILLONS', res.data)
            resolve(res.data)
          })
          .catch(err => {
            console.error(err)
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    loadPavillon: (store, pavillon) => {
      if (pavillon !== undefined && pavillon !== null) {
        MainStore.dispatch('loading', true)
        return new Promise((resolve, reject) => {
          axios.get('/pavillons/' + pavillon._id)
            .then(res => {
              store.commit('LOAD_PAVILLON', res.data)
              resolve(res.data)
            })
            .catch(err => {
              console.error(err)
              reject(err)
            })
            .then(_ => {
              MainStore.dispatch('loading', false)
            })
        })
      } else {
        store.dispatch('loadPavillons')
      }
    },
    addPavillon: (store, pavillon) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.put('/pavillons', pavillon)
          .then(res => {
            store.commit('ADD_PAVILLON', res.data)
            resolve(res.data)
          })
          .catch(err => {
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    removePavillon: (store, id) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.delete('/pavillons/' + id)
          .then(res => {
            store.commit('REMOVE_PAVILLON', id)
            resolve(res.data)
          })
          .catch(err => {
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    updatePavillon: (store, pavillon) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.put('/pavillons/' + pavillon._id, pavillon)
          .then(res => {
            store.commit('UPDATE_PAVILLON', pavillon)
            resolve(res.data)
          })
          .catch(err => {
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    setValidated: (store, pavillon) => {
      store.commit('SET_VALIDATED_PAVILLON', pavillon)
    }
  }
}
