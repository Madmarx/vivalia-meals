import MainStore from '@/store/Store'
import axios from '@/utils/axios'

export default {
  namespaced: true,
  state: {
    users: []
  },
  mutations: {
    LOAD_USERS (state, users) {
      state.users = users
    },
    ADD_USER (state, user) {
      state.users.push(user)
    },
    REMOVE_USER (state, id) {
      state.users = state.users.filter(p => p._id !== id)
    },
    UPDATE_USER: (state, user) => {
      state.users.forEach(p => {
        if (p._id === user._id) {
          p = user
          return null
        }
      })
    }
  },
  getters: {
    users: state => state.users,
    count: state => state.users.length,
    userByUsername: (state) => (username) => {
      return state.users.find(u => u.username === username)
    },
    userById: (state) => (id) => {
      return state.users.find(u => u._id === id)
    }
  },
  actions: {
    loadUsers: (store) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.get('/users')
          .then(res => {
            store.commit('LOAD_USERS', res.data)
            resolve(res.data)
          })
          .catch(err => {
            console.error(err)
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    loadUser: (store, user) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.get('/users/' + user._id)
          .then(res => {
            resolve(res.data)
          })
          .catch(err => {
            console.error(err)
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    addUser: (store, user) => {
      return new Promise((resolve, reject) => {
        axios.put('/users', user)
          .then(res => {
            store.commit('ADD_USER', res.data)
            resolve(res.data)
          })
          .catch(err => {
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    removeUser: (store, id) => {
      return new Promise((resolve, reject) => {
        axios.delete('/users/' + id)
          .then(res => {
            store.commit('REMOVE_USER', id)
            resolve(res.data)
          })
          .catch(err => {
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    updateUser: (store, user) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.put('/users/' + user._id, user)
          .then(res => {
            store.commit('UPDATE_USER', user)
            resolve(res.data)
          })
          .catch(err => {
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    }
  }
}
