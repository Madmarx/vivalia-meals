import Vue from 'vue'
import Vuex from 'vuex'

import AuthStore from './modules/auth/AuthStore'
import UsersStore from './modules/users/UsersStore'
import PavillonsStore from './modules/pavillons/PavillonsStore'
import DietsStore from './modules/diets/DietsStore'
import PatientsStore from './modules/patients/PatientsStore'
import MenusStore from './modules/menus/MenusStore'
import SideMenusStore from './modules/sideMenus/SideMenusStore'
import MenuVariantsStore from './modules/menus/MenuVariantsStore'
import MealsStore from './modules/meals/MealsStore'
import LotdStore from './modules/lotd/ListOfTheDayStore'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    appName: 'VivaliaMeals',
    loading: false
  },
  mutations: {
    LOADING (state, payload) {
      state.loading = payload
    }
  },
  getters: {
    appName: state => state.appName,
    loading: state => state.loading
  },
  actions: {
    loading: (store, payload) => {
      store.commit('LOADING', payload)
    }
  },
  modules: {
    AuthStore,
    UsersStore,
    PavillonsStore,
    PatientsStore,
    DietsStore,
    MenusStore,
    SideMenusStore,
    MenuVariantsStore,
    MealsStore,
    LotdStore
  },
  strict: true // forcer à utiliser le système de Store correctement (ne pas muter les stores à l'arrache)
})
